
package chat;

/**
 *
 * @author MIGUELNAVA
 */
public class ChatCliente extends javax.swing.JFrame {

    /**
     * Creates new form ChatCliente
     */
    public static ClientesHilo conexion;
        public ChatCliente() {
            initComponents();
        }
        
        void Envios(String s){
        String buffer;                              
            buffer = s;
            AreaM.setText(AreaM.getText()+"\n"+this.txtAreaEscribir.getText());
            if (buffer!=null){
                conexion.enviar(buffer);
                
                if (buffer.equals("Salir")){
                    AreaM.setText("Se ha finalizado la conexión");
                    
                    conexion.cerrar();                 
                }else{
                    
                }
            }
        }
        
        public static void iniciar(String ip){
            conexion = new ClientesHilo(ip);
            conexion.start();
        }
        
        
    

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBase = new javax.swing.JPanel();
        lbTitutoChat = new javax.swing.JLabel();
        scrollPaneChat = new javax.swing.JScrollPane();
        txtAreaChat = new javax.swing.JTextArea();
        scrollPaneUsuariosConectados = new javax.swing.JScrollPane();
        txtAreaUsuariosConectados = new javax.swing.JTextArea();
        lbUsuariosConectados = new javax.swing.JLabel();
        BotonEnviar = new javax.swing.JButton();
        scrollPaneEscribir = new javax.swing.JScrollPane();
        txtAreaEscribir = new javax.swing.JTextArea();
        txtDireccionIP = new javax.swing.JTextField();
        lbDireccionIP = new javax.swing.JLabel();
        BotonConectar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lbTitutoChat.setFont(new java.awt.Font("Arial", 1, 22)); // NOI18N
        lbTitutoChat.setText("MINICHAT");

        txtAreaChat.setColumns(20);
        txtAreaChat.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtAreaChat.setRows(5);
        scrollPaneChat.setViewportView(txtAreaChat);

        txtAreaUsuariosConectados.setColumns(20);
        txtAreaUsuariosConectados.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtAreaUsuariosConectados.setRows(5);
        scrollPaneUsuariosConectados.setViewportView(txtAreaUsuariosConectados);

        lbUsuariosConectados.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lbUsuariosConectados.setForeground(new java.awt.Color(0, 0, 153));
        lbUsuariosConectados.setText("Usuarios conectados");

        BotonEnviar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BotonEnviar.setForeground(new java.awt.Color(0, 204, 0));
        BotonEnviar.setText("Enviar");
        BotonEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonEnviarActionPerformed(evt);
            }
        });

        txtAreaEscribir.setColumns(20);
        txtAreaEscribir.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtAreaEscribir.setRows(5);
        scrollPaneEscribir.setViewportView(txtAreaEscribir);

        lbDireccionIP.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lbDireccionIP.setText("Escriba su direccion IP:");

        BotonConectar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BotonConectar.setForeground(new java.awt.Color(0, 0, 204));
        BotonConectar.setText("Conectar");
        BotonConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonConectarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBaseLayout = new javax.swing.GroupLayout(panelBase);
        panelBase.setLayout(panelBaseLayout);
        panelBaseLayout.setHorizontalGroup(
            panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelBaseLayout.createSequentialGroup()
                        .addComponent(scrollPaneEscribir, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BotonEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtDireccionIP, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbDireccionIP, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(BotonConectar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBaseLayout.createSequentialGroup()
                        .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(scrollPaneChat)
                            .addGroup(panelBaseLayout.createSequentialGroup()
                                .addGap(284, 284, 284)
                                .addComponent(lbTitutoChat)
                                .addGap(0, 55, Short.MAX_VALUE)))
                        .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelBaseLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(scrollPaneUsuariosConectados, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBaseLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbUsuariosConectados)
                                .addGap(53, 53, 53))))))
        );
        panelBaseLayout.setVerticalGroup(
            panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBaseLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitutoChat)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelBaseLayout.createSequentialGroup()
                        .addComponent(lbUsuariosConectados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPaneUsuariosConectados, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scrollPaneChat, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(scrollPaneEscribir, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(BotonEnviar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelBaseLayout.createSequentialGroup()
                        .addComponent(lbDireccionIP)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDireccionIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BotonConectar))))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonEnviarActionPerformed
        Envios(this.txtAreaEscribir.getText());
        this.txtAreaEscribir.setText("");
    }//GEN-LAST:event_BotonEnviarActionPerformed

    private void BotonConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonConectarActionPerformed
        iniciar(this.txtDireccionIP.getText());
    }//GEN-LAST:event_BotonConectarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChatCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChatCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChatCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChatCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChatCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonConectar;
    private javax.swing.JButton BotonEnviar;
    private javax.swing.JLabel lbDireccionIP;
    private javax.swing.JLabel lbTitutoChat;
    private javax.swing.JLabel lbUsuariosConectados;
    private javax.swing.JPanel panelBase;
    private javax.swing.JScrollPane scrollPaneChat;
    private javax.swing.JScrollPane scrollPaneEscribir;
    private javax.swing.JScrollPane scrollPaneUsuariosConectados;
    private javax.swing.JTextArea txtAreaChat;
    private javax.swing.JTextArea txtAreaEscribir;
    private javax.swing.JTextArea txtAreaUsuariosConectados;
    private javax.swing.JTextField txtDireccionIP;
    // End of variables declaration//GEN-END:variables

    static class AreaM {

        static void setText(String chat_ITVer) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        static String getText() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public AreaM() {
        }
    }
}
