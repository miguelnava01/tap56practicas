package chat;

import static chat.ChatCliente.AreaM;
import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MIGUELNAVA
 */
public class ClientesHilo extends Thread {
    Socket ClienteSocket;
    PrintWriter out;
    BufferedReader in;              
    String Buffer;
    SimpleDateFormat formatterMDY;
    boolean Salir = false;
    String IP;
    
    public ClientesHilo(String IP){
        IP=IP;
    }
    
    public void run(){        
        String fechaHora;
        ChatCliente.AreaM.setText("Bienvenido al Chat, escriba su nombre");
        formatterMDY = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            
        try {
            ClienteSocket = new Socket(IP,3333);

            out =
                    new PrintWriter(ClienteSocket.getOutputStream(), true);
            
            in = new BufferedReader(
                    new InputStreamReader(ClienteSocket.getInputStream()));  
            
            while(!this.Salir){                
                Buffer = in.readLine();
                
                if (Buffer!=null){
                    fechaHora = formatterMDY.format(Calendar.getInstance().getTime());
                    ChatCliente.AreaM.setText(AreaM.getText()+"\n"+fechaHora+ " < "+Buffer);
                }                
            }                        
        } catch (IOException ex) {
            Logger.getLogger(ClientesHilo.class.getName()).log(Level.SEVERE, null, ex);
        }                           
    }
    void enviar(String mensaje) {
        out.println(mensaje);
    }

    void cerrar() {
        try {
            ClienteSocket.close();
            in.close();
            out.close();
            this.Salir = true;
        } catch (IOException ex) {
            Logger.getLogger(ClientesHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}