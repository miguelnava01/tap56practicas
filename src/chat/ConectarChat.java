package chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author Zorge
 */
public class ConectarChat extends javax.swing.JFrame {
    
    public ConectarChat() {
        initComponents();
    }
    Socket conexion;
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpBase = new javax.swing.JPanel();
        lbIcono = new javax.swing.JLabel();
        lbDireccionIP = new javax.swing.JLabel();
        txtDireccionIP = new javax.swing.JTextField();
        lbNombreCompleto = new javax.swing.JLabel();
        txtNombreCompleto = new javax.swing.JTextField();
        BotonConectar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lbIcono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/iconochat.jpg"))); // NOI18N

        lbDireccionIP.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lbDireccionIP.setText("Ingresar la direccion IP:");

        txtDireccionIP.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtDireccionIP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDireccionIPActionPerformed(evt);
            }
        });

        lbNombreCompleto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lbNombreCompleto.setText("Ingresar su nombre completo:");

        txtNombreCompleto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtNombreCompleto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreCompletoActionPerformed(evt);
            }
        });

        BotonConectar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BotonConectar.setForeground(new java.awt.Color(0, 0, 204));
        BotonConectar.setText("Conectar");
        BotonConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonConectarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpBaseLayout = new javax.swing.GroupLayout(jpBase);
        jpBase.setLayout(jpBaseLayout);
        jpBaseLayout.setHorizontalGroup(
            jpBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpBaseLayout.createSequentialGroup()
                .addGap(0, 95, Short.MAX_VALUE)
                .addGroup(jpBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbNombreCompleto)
                    .addComponent(txtDireccionIP, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbDireccionIP, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbIcono))
                .addGap(88, 88, 88))
            .addGroup(jpBaseLayout.createSequentialGroup()
                .addGap(164, 164, 164)
                .addComponent(BotonConectar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpBaseLayout.setVerticalGroup(
            jpBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpBaseLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbIcono)
                .addGap(27, 27, 27)
                .addComponent(lbDireccionIP)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtDireccionIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(lbNombreCompleto)
                .addGap(18, 18, 18)
                .addComponent(txtNombreCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(BotonConectar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtDireccionIPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDireccionIPActionPerformed
        try {
	    String thisIp = InetAddress.getLocalHost().getHostAddress();
	    System.out.println("IP:"+thisIp);
        }catch(Exception e) {
	    e.printStackTrace();
        }
    }//GEN-LAST:event_txtDireccionIPActionPerformed

    private void txtNombreCompletoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreCompletoActionPerformed
        String texto[];
        txtNombreCompleto campo = new txtNombreCompleto(texto);
    }//GEN-LAST:event_txtNombreCompletoActionPerformed

    private void BotonConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonConectarActionPerformed
        class ProcesoHilo extends Thread {
            ProcesoHilo(Socket cnx){
            conexion = cnx;
        }
            
            public void run(){
                String msgEnviar = "";
                String msgRespuesta = "";
                boolean bSalir = false;
                
                try {      
                    System.out.printf("Recibiendo conexion desde %s.\n",conexion.getInetAddress().toString());
            
                    // Canal para la entrada
                    InputStream is = conexion.getInputStream();
                    DataInputStream dis = new DataInputStream(is);
                    // Canal para la salida
                    OutputStream os = conexion.getOutputStream();            
                    DataOutputStream dos = new DataOutputStream(os);
            
                    dos.writeUTF("Bienvenido a Servidor Chat. Escriba su nombre ");
            
                    msgRespuesta = dis.readUTF();
            
                    System.out.printf("Se conecto: %s.\n",msgRespuesta); 
            
                    while (!bSalir){                
                        msgEnviar = "Respuesta eco: "+msgRespuesta;                 
                        dos.writeUTF(msgEnviar);
                                  
                        msgRespuesta = dis.readUTF();                
                        System.out.printf("Recibiendo desde %s - %s",conexion.getInetAddress().toString(),msgRespuesta);
                 
                        if(msgRespuesta.equals("salir")){
                            bSalir = true;
                        }
                    }                                             
                        conexion.close();
                } catch (IOException io){
            
                }
            }
        }
    }//GEN-LAST:event_BotonConectarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConectarChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConectarChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConectarChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConectarChat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConectarChat().setVisible(true);
            }
        });
       
    }
            
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonConectar;
    private javax.swing.JPanel jpBase;
    private javax.swing.JLabel lbDireccionIP;
    private javax.swing.JLabel lbIcono;
    private javax.swing.JLabel lbNombreCompleto;
    public static javax.swing.JTextField txtDireccionIP;
    public static javax.swing.JTextField txtNombreCompleto;
    // End of variables declaration//GEN-END:variables

    private static class txtNombreCompleto {

        public txtNombreCompleto() {
        }
    }
}